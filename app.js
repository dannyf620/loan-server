const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const docTypeRouter = require('./routes/documentTypeRouter')
const users = require('./routes/user');
const cors = require('cors');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const utility = require('./utility');
// const restify = require('express-restify-mongoose');
const jsonServer = require('json-server');

utility.validateDbFiles();
const url = 'mongodb://localhost:27017/zinobe';
const connect = mongoose.connect(url);
connect.then((db) => {
  console.log("Connected correctly to server");
}, (err) => { console.log(err); });

const app = express();

app.use(bodyParser.json());
app.use(methodOverride());

mongoose.connect('mongodb://localhost:27017/zinobe',  { useNewUrlParser: true });

// const router = express.Router();

// restify.serve(router, mongoose.model('Customer', new mongoose.Schema({
//   name: { type: String, required: true },
//   comment: { type: String }
// })));


// app.use(router);
// view engine setup
app.use(cors());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/users', users);
app.use('/doc-type',docTypeRouter);
app.use('/api', jsonServer.router('jsonServer/db.json'));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
