const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const documentTypeSchema = new Schema({
    type: {
        type: String,
        required: true
    },
    name: { type: String, required: true }
}, {
        timestamps: true
    });


module.exports = mongoose.model('DocumentTypes', documentTypeSchema);
