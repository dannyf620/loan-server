const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const loanStatesSchema = new Schema({
    type: String,
    name: String,
    description: String
}, {
    timestamps: true
});

module.exports = mongoose.model('LoanStates', loanStatesSchema);
