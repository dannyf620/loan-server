const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loanSchema = new Schema({
    startDate:{
        type: Date,
        required: true,
        unique: true
    },
    endDate:{
        type: Date,
        required: true,
        unique: true
    },
    whoApproved:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    loanState:{
        type: String,
        required: true,
        unique: true
    },
    completed:{
        type: Boolean,
        required: true,
        unique: true
    },
    duration:{
        type: Number,
        required: true,
        unique: true
    },
    annualRate:{
        type: Number,
        required: true,
        unique: true
    }
},{
    timestamps: true
});

let Loans = mongoose.model('Loans', loanSchema);

module.exports = Loans;
