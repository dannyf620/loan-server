const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');

let User = new Schema({
    username: {
        type: String,
        required: true
    },
    password: String,
    OauthId: String,
    OauthToken: String,
    resetPasswordExpires: Date,
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    documentType:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'DocumentTypes'
    },
    documentNumber:{
        type: Number
    },
    phoneNumber: Number,
    //userType can be 'User','Owner','Admin','Engineer','Bouncer'
    userType: {
        type: [String],
        default: ['User']
    },
    email: {
        type: String,
        required: true
    },
    userState: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserStates'
    },

    loans:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Loans'
    }],
    roles:{
        type: [String],
        default: ['User']
    }
}, {
    timestamps: true
});

User.methods.getName = function () {
    return (this.firstname + ' ' + this.lastname);
};

User.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', User);
