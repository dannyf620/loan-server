const express = require('express');
const bodyParser = require('body-parser');
const documentTypes = require('../models/documentTypes');
const Verify = require('./verify');

const documentTypeRouter = express.Router();
documentTypeRouter.use(bodyParser.json());

documentTypeRouter.route('/')
    .get(function (req, res, next) {
        documentTypes.find(req.query)
            .exec(function (err, types) {
                if (err) next(err);
                res.json(types);
            });
    })
    .post( function (req, res, next) {
        documentTypes.create(req.body, function (err, docmentType) {
            if (err) next(err);
            // const id = docmentType._id;
            res.json(docmentType);
        });
    })
    .delete(Verify.verifyOrdinaryUser, Verify.verifyEngineer, function (req, res, next) {
        const userId = req.decoded._id;

        documentTypes
            .findOneAndRemove({
                adminBy: userId
            }, function (err, resp) {
                if (err) next(err);
                res.json(resp);
            });
    });

module.exports = documentTypeRouter;
