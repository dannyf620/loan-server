const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Loans = require('../models/loans');
const Verify = require('./verify'); // i added this and it looks good

const loanRouter = express.Router();
loanRouter.use(bodyParser.json());

loanRouter.route('/')
    .get(function (req, res, next) {
        Loans.find(req.query)
            .exec(function (err, loan) {
                if (err) next(err);
                res.json(loan);
            });
    })

    .post(function (req, res, next) {
        console.log(req.decoded._id);
        Loans.create(req.body, function (err, loan) {
            if (err) next(err);
            const id = loan._id;
            res.json({ "_id": id });
        });
    });


loanRouter.route('/:loanId')
    .get(function (req, res, next) {
        Loans.findById(req.params.loanId)
            .exec(function (err, loan) {
                if (err) next(err);
                res.json(loan);
            });
    })

    .put(function (req, res, next) {
        Loans.findByIdAndUpdate(req.params.loanId, {
            $set: req.body
        }, {
            new: true
        }, function (err, loan) {
            if (err) next(err);
            res.json(loan);
        });
    })

    .delete(Verify.verifyAdmin, function (req, res, next) {
        Loans.findByIdAndRemove(req.params.loanId, function (err, resp) {
            if (err) next(err);
            res.json(resp);
        });
    });

module.exports = loanRouter;
