const express = require('express');
const router = express.Router();
const passport = require('passport');
const User = require('../models/user');
const Verify = require('./verify');

router.get('/', Verify.verifyOrdinaryUser, Verify.verifyEngineer, function (req, res, next) {
    User.find({}, function (err, user) {
        if (err) {
            return res.status(500).json({
                err: 'not enough privileges '
            });
        }
        res.json(user);
    });
});


router.post('/register', function (req, res) {
    User.register(new User({ username: req.body.username.toLowerCase(), email: req.body.email }),
        req.body.password, function (err, user) {
            if (err) {
                console.log(err);
                return res.status(500).json({ err: err });
            }
            if (req.body.firstname) {
                user.firstname = req.body.firstname;
            }
            if (req.body.lastname) {
                user.lastname = req.body.lastname;
            }
            if (req.body.email) {
                user.email = req.body.email;
            }
            if (req.body.documentType) {
                user.documentType = req.body.documentType;
            }
            if (req.body.documentNumber) {
                user.documentNumber = req.body.documentNumber;
            }
            if (req.body.phoneNumber) {
                user.phoneNumber = req.body.phoneNumber;
            }
            user.save(function (err, userSaved) {
                req.body.username = req.body.username.toLowerCase();
                passport.authenticate('local')(req, res, function () {
                    return res.status(200).json({ status: 'Registro Exitoso!' });
                });
            });
        });
});

router.post('/login', function (req, res, next) {
    req.body.username = req.body.username.toLowerCase();
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return next(err);
        }
        console.log(user);
        if (!user) {
            return res.status(401).json({
                err: info
            });
        }
        req.logIn(user, function (err) {
            if (err) {
                return res.status(500).json({
                    err: 'Could not log in user'
                });
            }

            const token = Verify.getToken({ "username": user.username.toLowerCase(), "_id": user._id, userType: user.userType });
            res.status(200).json({
                status: 'Login successful!',
                success: true,
                userType: user.userType,
                token: token
            });
        });
    })(req, res, next);
});

router.get('/logout', function (req, res) {
    req.logout();
    res.status(200).json({
        status: 'C ya!'
    });
});
router.get('/exist', function (req, res, next) {
    User.findOne({ username: req.query.username.toLowerCase() })
        .exec(function (err, person) {
            if (err) next(err);
            if (person)
                res.json({ exist: true });
            else
                res.json({ exist: false });
        });
});

router.get('/facebook', passport.authenticate('facebook'),
    function (req, res) { });

router.get('/facebook/callback', function (req, res, next) {
    passport.authenticate('facebook', function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.status(401).json({
                err: info
            });
        }
        req.logIn(user, function (err) {
            if (err) {
                return res.status(500).json({
                    err: 'Could not log in user'
                });
            }
            const token = Verify.getToken(user);
            res.status(200).json({
                status: 'Login successful!',
                success: true,
                username: user.username,
                token: token
            });
        });
    })(req, res, next);
});
//cambiar correo -> emailResponse
router.post('/emailResponse', function (req, res, next) {
    User.findOne({ email: req.body.email }, function (error, user) {
        if (!user) {
            return res.status(404).json({
                err: 'Usuario no encontrado'
            });
        } else {
            // TODO: envio de correo
            res.json({ "Id": user._id });
        }
    });
});
router.get('/changepass/:userId', function (req, res, next) {
    User.findById(req.params.userId)
        .exec(function (err, user) {
            if (err) { next(err) } else {
                const d = new Date() - 60000 * 40;
                console.log(user.resetPasswordExpires - d);
                if (user.resetPasswordExpires - d > 0) {
                    res.send({ expired: false, "username": user.username });
                } else {
                    res.send({ expired: true, "username": user.username });
                }


                //res.json(user);
            }
        });
});
router.post('/changepass/:userId', function (req, res, next) {
    User.findById(req.params.userId).then(function (sanitizedUser) {
        if (sanitizedUser) {
            sanitizedUser.setPassword(req.body.password, function () {
                sanitizedUser.save();
                res.status(200).json({ message: 'password reset successful' });
            });
        } else {
            res.status(500).json({ message: 'This user does not exist' });
        }
    }, function (err) {
        console.error(err);
    })
});

module.exports = router;
