const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('../config.js');

exports.getToken = function (user) {
    return jwt.sign(user, config.secretKey, {
        expiresIn: "35d"
    });
};

exports.verifyOrdinaryUser = function (req, res, next) {
    // check header or params
    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
        jwt.verify(token, config.secretKey, function (err, decoded) {
            if (err) {
                const err = new Error('You are not authenticated!');
                err.status = 401;
                return next(err);
            } else {
                req.decoded = decoded;

                next();
            }
        });
    } else {
        const err = new Error('No token provided!');
        err.status = 403;
        return next(err);
    }


};

exports.verifyAdmin = function (req, res, next) {
    if (!req.decoded) {
        const err = new Error('Usted no está autorizado para realizar esta operació');

        //not admin pass error to next middleware
        err.status = 403;
        return next(err);
    } else {
        const id = req.decoded._id;

        if (!req.decoded.roles.includes("Admin")) {
            const err = new Error('Usted no está autorizado para realizar esta operación');
            err.status = 403;
            return next(err);
        } else

            next();
    }

};

exports.verifyEngineer = function (req, res, next) {
    if (!req.decoded) {
        const err = new Error('No es un usuario, porfavor Registrese');

        //not admin pass error to next middleware
        err.status = 403;
        return next(err);
    } else {
        const id = req.decoded._id;
        if (!req.decoded.roles.includes("Engineer")) {
            const err = new Error('No esta autorizado para realizar esta acción');
            err.status = 403;
            return next(err);
        } else

            next();
    }

};
