const fs = require('fs');
const path = require('path');
module.exports = {
    validateDbFiles() {

        let files = fs.readdirSync('jsonServer.base');
        files.forEach(filename => {
            if (filename.indexOf('.json') !== -1) {
                let name = filename;
                name = name.substr(0, name.lastIndexOf('.'));
                let filePathCopy = path.resolve('./jsonServer') + '/' + filename;
                let filePathOriginal = path.resolve('./jsonServer.base') + '/' + filename;

                // if (!fs.existsSync(filePathCopy)) {
                //      fs.readFile(filePathOriginal, (err, data) => {
                //         if (err) throw err;
                //          fs.writeFile(filePathCopy, data.toString(), (err) => {
                //             if (err) {
                //                 return console.log(err);
                //             }
                //         });

                //     });
                // }


                if (!fs.existsSync(filePathCopy)) {
                    fs.writeFileSync(filePathCopy, fs.readFileSync(filePathOriginal));
                }


            }
        });


    }
}